# DISRUPTING DISRUPTIVE INNOVATION WITH / IN LARGE LANGUAGE MODELS

## About

This is a small experimental project by Evert van Beek. It uses the OpenAI api and gradio to create a simple interface where you can ask a large language model about the next disruptive innovation in a specific industry. It will then come up with something different from what you asked for (sometimes nonsense, sometimes something new and interesting).

This is a critical (and somewhat parodic) design experiment about the biases and fixations in large language models and how creative practices intersect with AI. It uses a set of completion prompts that forces GPT-3 to come up with new hypes and innovations that are not in its dataset. There is also a simple web interface which enables you to enter an industry or field, after which it returns an idea for a disruptive innovation. This idea and its description will be nonsensical, but still reflect the vocabulary and ideas of trends and innovations. The experiment raises many questions (and hopefully some awareness) about creativity, surprise, technical progress and subversion.

## Requirements and getting started

Running the code requires Python, and a few packages installed (optionally [in a virtual environment](https://docs.python.org/3/library/venv.html)).

To install the required packages, type this into the terminal:

    pip install openai
and

    pip install gradio

You also need an OpenAI api key, [which you can get here](https://platform.openai.com/). Copy and paste the key (starting with 'sk-') into the file api.py.

Now you can run disruptionsfordistribution.py from the terminal, like this:

    python /your/path/to/the/file/disruptionsfordistribution.py

If everything went well, this will return the following: "Running on local URL: ...."
Copy and paste this URL in the browser to open the interface.

## The prompts

1. Get five supposedly disruptive technologies.

    'Five disruptive digitalization technologies in '+industry+' are:
    
    Technology: Explanation of the technology.
    
    1.'
2. Split the name in two, in case there is no space in the name (e.g., blockchain).

    'Split the following word in two: '+tech+'.'
3. Come up with a synonym and antonym for the two parts of the name, and select a random combination.

    'Give five synonyms and five antonyms to each of the following words: '+tech+'.
    
    Then combine a random antonym or synonym of the first word, with a random antonym or synonym of the second word. Etc.'
4. Explain the new thing with reference to the original explanation.

    explanation+'\n\n'+techresult[1:]+', on the other hand,'

## Update Jan. 2024: Worse performance with newer models

Jan. 4th 2024, OpenAI [shut down](https://openai.com/blog/gpt-4-api-general-availability) older models including text-davinci-003 which was used for this project. The code runs fine with the suggested replacement (gpt-3.5-turbo-instruct, a chat completions model) but it has become less likely to produce novel and interesting results (i.e. this model is 'more aligned' but thereby also 'more boring'). It might be interesting to play around with temperatures in the different prompts.

Another 'true' (non-chat) text completion model by OpenAI is davinci-002. Unfortunately, davinci-002 does not perform well in this setup.