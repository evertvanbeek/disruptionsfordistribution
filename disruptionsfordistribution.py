# importing the necessary packages, see the readme for how to install them
import gradio as gr
import openai
import random
import re

# importing the api key, see the readme for how to get one and where to put it
from api import key
openai.api_key = key

# A: the function that does all the work (takes 'industry' as input, and returns a strange disruptive innovation in that industry).
def webprompt(industry):
    
    # 1. Initial prompt to get five supposedly disruptive technologies from the LLM at the OpenAI api. 
    prompt_a = 'Five disruptive digitalization technologies in '+industry+' are:\n\n0. Technology: Explanation of the technology.\n\n1.'    # the initial prompt to the api. formatted in a specific way to always return the 
    response_a = openai.Completion.create(
        engine='gpt-3.5-turbo-instruct',
        prompt=prompt_a,
        max_tokens=256,
        temperature=0.7,
    )

    # extract the text from the response
    completion_text = response_a['choices'][0]['text']
    disruptivefive = '1.'+ completion_text
	
    # now some initial processing to select one of the random responses
    listoffive = disruptivefive.split("\n\n")                          # split at the enters
    filteredlist = list(filter(lambda x: x.endswith('.'), listoffive)) # remove the ones that do not end with a "." (as descriptions sometimes consist out of more than one line)
    selected = random.choice(filteredlist)                             # select a random
    selectedlist = selected.split(": ")				       # split between the name of the technology and its description
    explanation = selectedlist[1]
    tech = re.sub(r'(\d{1})\. ', '', selectedlist[0])                  #removing the number indicator
    tech = re.sub(r'\([^)]*\)', '', tech)                  	       #removing what's between brackets

    # 2. We want to make random combinations of two antonyms and synonyms for two parts of the name of a technology (e.g., Augmented Reality becomes Improved Fiction),
    # but sometimes the name of the technology does not contain a space yet, or is not a two part term yet (e.g., Blockchain). We ask the LLM api to split the name in two.
    if not" " in tech:
        prompt_b = 'Split the following word in two: '+tech+'.'
        response_b = openai.Completion.create(
            engine='gpt-3.5-turbo-instruct',
            prompt=prompt_b,
            max_tokens=60,
            temperature=0.2,
        )
        response_b = response_b['choices'][0]['text']
        response_b = re.sub('[^0-9a-zA-Z]+', ' ', response_b)        # replace +,-,•, etc. with a space (as the LLM sometimes returns the two parts of the name with a + or the word 'and' in between (e.g., "block and chain")
        response_b = re.sub(r"\b and \b", ' ', response_b)           # replace ' and ' with a space
        tech = response_b

    tech = tech.replace(" ",", ")                                    #insert a comma for the next prompt

    # 3. The LLM comes up with five synonyms and five antonyms for each of the two parts of the name of a technology (e.g., five for 'augmented', and five for 'reality).
    # Immediately, we ask it to select random results and combine them into one.
    prompt_c = 'Give five synonyms and five antonyms to each of the following words: '+tech+'.\nThen combine a random antonym or synonym of the first word, with a random antonym or synonym of the second word. Etc.'
    response_c = openai.Completion.create(
        engine='gpt-3.5-turbo-instruct',
        prompt=prompt_c,
        max_tokens=260,
        temperature=0.5,
    )
    synanto = response_c['choices'][0]['text']

    # We need to do some more processing of the text to actually select the right part of the output. We only need the two words (e.g., Improved Fiction) which we store in the variable tech result.
    index = synanto.rfind(': ')
    if index == -1:                                    # if there is no : in the string, we continue with everything
        result = synanto
    else:
        synanto = synanto[synanto.rfind(': '):]        # remove everything before
        synanto = re.sub(r"\b and \b", ' ', synanto)   # replace ' and ' with a space
        result = synanto.replace(': ','')              # remove the : 

    result = re.sub(r"\b\n\n\b", ' ', result)          # in case there's a new line, we need to replace it with a space
    salist = result.split(" ")                         # split at the spaces (with every new word)


    if len(salist) >= 4:
        synanto_a = str(salist[len(salist) - 2])       # select the second to last word
        synanto_b = str(salist[len(salist) - 1])       # select the last word
        salist = [synanto_a, synanto_b]
    else:
        salist = salist

    salist = [s.strip('.') for s in salist]            # in case there's a '.', we need to remove it

    #turn list into (short) string
    techresult=''
    for x in salist:
        techresult += ' '+ x

    # 4. In the final step, we give the LLM the original selected technology (e.g., Augmented Reality) and the explanation of that technology it gave earlier.
    # Then we ask it to explain what the new technology (e.g., Improved Fiction) could be. This prompt structure makes sure that the LLM will use hype terminology from disruptive innovation discourse.
    prompt_d = explanation+'\n\n'+techresult[1:]+', on the other hand,'

    response_d = openai.Completion.create(
        engine='gpt-3.5-turbo-instruct',
        prompt=prompt_d,
        max_tokens=200,
        temperature=0.5,
        stop=["\n"]
    )

    newexplan = response_d['choices'][0]['text']
    finalres = techresult[1:] + newexplan

    # This last part is just to make sure that the result will look nice in the interface (it refers to some html-styling from the css file).
    htmlresult = "<disstyle>" + finalres + "</disstyle>"
    return htmlresult

# B: the function that makes it look nice in an interface. This is based on the gradio blocks.

with gr.Blocks(css = "stylesimp.css") as webdisrupt:
    gr.HTML("<toptitle><i><b>Disrupting</b></i> <s>disruptive innovation</s> </toptitle>")
    gr.HTML("<sectitle>Using Large Language Models to subvert and interrogate dominant narratives in innovation</sectitle>")
    textinput = gr.Textbox(lines=1, placeholder="e.g. automotive, wine making, building renovations, ...", label="In what industry are you?")
    disrupt_btn = gr.Button("Disrupt!")
    gr.HTML("<sectitle>...then this will not be the next disruptive innovation:</sectitle>")
    output = gr.HTML()
    disrupt_btn.click(fn=webprompt, inputs=textinput, outputs=output)
    gr.HTML("<bottomtext>Uses OpenAI API. Work in progress by Evert van Beek | <a href='https://mastodon.nl/@everyt'>@everyt@mastodon.nl</a> | <a href='https://www.evertvanbeek.com'>contact@evertvanbeek.com</a></bottomtext>")

# C: launching the interface (change to share=True if you want a link to share with others)
webdisrupt.launch(
    share=False
)